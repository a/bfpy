# BFPY

A rather slow brainfuck interpreter, written for Python 3.

This is my first ever attempt at writing an interpreter, so uh, I guess it didn't turn out as bad as I expected, but it's most definitely not great.

It supports configuring a bunch of implementation details, like cell size (aka tape size), wrapping, input EOF character etc. See `config.py` for the whole list.

---

## Pictures

![Rot13 of "Hewwo world" with bfpy using a rot13 brainfuck program](https://asa.cease-and-desisted.me/i/65pem2y4.png)

![](https://asa.cease-and-desisted.me/i/55j9m9tn.png)

---

## Usage

```
usage: bfpy.py [-h] [--input INPUT] [filename]

Shitty brainfuck interpreter

positional arguments:
  filename       filename for the bf to be loaded from

optional arguments:
  -h, --help     show this help message and exit
  --input INPUT  input to give to the program
  ```