#!/bin/env python3
import argparse
from config import *
import re
import sys

# Shitty BF interpreter by aveao, 2018
# This is SLOOOOWWWWW
# See config.py for config options

bf_validre = re.compile(r"[^\>\+\<\-\.\,\[\]]+")


parser = argparse.ArgumentParser(description='Shitty brainfuck interpreter')
parser.add_argument('filename', metavar='filename', type=str, nargs='?',
                    help='filename for the bf to be loaded from')
parser.add_argument('--input', dest='input', action='store',
                    help='input to give to the program')

args = parser.parse_args()

# Use arguments if supplied, if not use default values
bf_code = default_bf
if args.filename:
    with open(args.filename) as file:
        bf_code = file.read()
bf_input_text = args.input if args.input else default_input

# Turn string into chars, then those to bytes, then those to ints
bf_input = [int.from_bytes(inpchar.encode(), byteorder=input_endianness) for
            inpchar in list(bf_input_text)]
# and finally reverse the input so that pops come from start
bf_input.reverse()

# Store BF output (when live_output is False)
bf_output = ""

# Define valid commands so we can ignore the rest
code_pos = 0

# It's a bit hacky to store the cells like this but eh, too lazy
cells = {0: 0}
pointer_pos = 0

# real hacky stuff to get [ and ] working properly
jumpthrough = 0
if_pos = []


# Removes non-valid characters from BF to speed it up
bf_code = bf_validre.sub("", bf_code)


def cell_functions(cell_num):
    global pointer_pos
    if cell_num < 0 and not allow_sub_zero_cells:
        raise Exception("Program attempted to move below cell num 0")

    # Wrap cell size
    if cell_size and pointer_pos > cell_size:
        pointer_pos = pointer_pos - cell_size

    if cell_num not in cells:
        cells[cell_num] = 0


def wrap_functions(cell_num):
    global cells
    cell = cells[cell_num]
    # Wrap cell value (positive)
    if bf_wrap and cell > (bf_wrap - 1):
        cell = -bf_wrap + (cell - bf_wrap)
        cells[pointer_pos] = cell

    # Wrap cell value (negative)
    if bf_wrap and cell < -bf_wrap:
        cell = bf_wrap + (cell + bf_wrap)
        cells[pointer_pos] = cell


while code_pos < len(bf_code):
    # Get the character from code
    character = bf_code[code_pos]

    # Get the cell at the pointer's position to be able to read it easily
    pointer_cell = cells[pointer_pos]
    if debug:
        print(f"{character} - cp:{code_pos} - pp:{pointer_pos} "
              f"- pc:{pointer_cell} - cl:{cells} - jt:{jumpthrough} "
              f"- ip: {if_pos}")

    if jumpthrough:
        if character == '[':
            jumpthrough += 1
        elif character == ']':
            jumpthrough -= 1
        code_pos += 1
        continue

    if character == '+':
        cells[pointer_pos] += 1
        wrap_functions(pointer_pos)
    elif character == '-':
        cells[pointer_pos] -= 1
        wrap_functions(pointer_pos)
    elif character == '>':
        pointer_pos += 1
        cell_functions(pointer_pos)
    elif character == '<':
        pointer_pos -= 1
        cell_functions(pointer_pos)
    elif character == '.':
        if live_output:
            sys.stdout.write(chr(pointer_cell))
        else:
            bf_output += chr(pointer_cell)
    elif character == '[':
        if not pointer_cell:
            jumpthrough += 1
        else:
            if_pos.append(code_pos)
    elif character == ']':
        if pointer_cell:
            code_pos = if_pos[-1] - 1
        if_pos.remove(if_pos[-1])
    elif character == ',':
        # If there's any input left, put that to cell, else put bf_input_eof
        cells[pointer_pos] = bf_input.pop() if bf_input else bf_input_eof

    code_pos += 1

if not live_output:
    print(bf_output)
