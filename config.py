debug = False
# "No file supplied. (see --help)"
default_bf = "-[--->+<]>-------.-[--->+<]>.[--->+<]>-----.++[->+++<]>.+++.+++.-------.--[--->+<]>-.---[->++++<]>-.++.-----..----.---.----.-.[->+++<]>++.++[--->++<]>.++++++++.--[->+++<]>+.++++[->+++<]>..--[--->+<]>-.--[-->+++<]>..[->++++++++<]>.---.+++++++.++++.[++>---<]>+.>++++++++++."
default_input = "test 123"

# To output as . commands come in, or to output them once program is done
# Setting to True breaks animation and progress,
# but it might improve performance
live_output = True

# There's no standard on many of brainfuck's implementation details
# (which makes it a brainfuck to implement it)
# Here's some config values you can configure to run
# programs that rely on certain settings
cell_size = 30000  # set to 0 to disable cell size limit
allow_sub_zero_cells = True
bf_input_eof = 0
bf_wrap = 128  # set to 0 to disable wrapping
input_endianness = 'little'
